<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use RealRashid\SweetAlert\Facades\Alert;

class PostsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = auth()->user()->following()->pluck('profiles.user_id');

        $posts = Post::whereIn('user_id', $users)->latest()->paginate();

        Alert::success('Success Title', 'Success Message');

        return view('posts.index', compact('posts'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store()
    {
        $data =request()->validate([
            'caption' => 'required',
            'image' => ['required','image'],
        ]);

    $imagePath = request('image')->store('uploads', 'public');

    $image = Image::make(public_path("storage/{$imagePath}"))->fit(1200,1200);
    $image->save();

        // $post = new \App\Models\Post();

        // $post->caption = $data['caption'];
        // $post->save();

        auth()->user()->posts()->create([
            'caption' => $data['caption'],
            'image' => $imagePath,
        ]);

        return redirect('/profile/'.auth()->user()->id); //ini '/profile/ ga salah, karna redirect auth nya dijadiin 1
        //beda sama profilescontroller.php

    
    }

    public function show(\App\Models\Post $post)
        {
            return view('posts.show', compact('post'));
        }
}
