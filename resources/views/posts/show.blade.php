@extends('layouts.app')

@section('content')
<div class="container" style="max-width: 1000px;">
  <div class="row no-gutters">
    <div class="mx-auto d-block col-4">
      <img src="/storage/{{ $post->image }}" class="w-100" style="width:50%">
    </div>
    <div class="col-8">
      <div class="card-body">
        <div class="d-flex align-items-center">
          <div class="pr-3">
            <img src="{{ $post->user->profile->profileImage() }}" class="rounded-circle w-100" style="max-width: 40px">
          </div>
              <div>
                <div class="font-weight-bold">
                    <a href="/profile/{{ $post->user->id }}">
                      <span class="text-dark">{{ $post->user->username }}</span>
                    </a>
                    <a href="#" class="pl-3">Follow</a>
                </div>
              </div>
          </div>

          <hr>

          <p>
            <span class="font-weight-bold">
                <a href="/profile/{{ $post->user->id }}">
                  <span class="text-dark">{{ $post->user->username }}</span>
                </a>
            </span> {{ $post->caption }}
          </p>
      </div>
    </div>
  </div>
</div>
@endsection

